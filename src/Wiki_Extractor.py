#!/usr/bin/env python 
from bs4 import BeautifulSoup
import json
import urllib2 as ur
import sys
import lxml
import os
import pymongo
from pymongo import MongoClient

reload(sys)
sys.setdefaultencoding('utf8')

class WikiExtractor:
    def __init__(self, filename):
            self.filename = filename
            self.url = ''
            self.cnt = 0
            self.document_list = list()
            self.client = MongoClient('localhost', 27017)
            self.db = self.client.WikiDb
            self.doc = self.db.Documents

    def get_url(self):
        with open(self.filename, 'r+') as input_file:
            for line in input_file.readlines():
                # print line
                for url in line.strip().split():
                    # print url
                    if url.startswith('http'):
                        self.url = url
                        # print self.url
                        self._get_url(self.url)
        

    def _get_url(self, url):
        try:
            document_name = os.path.basename(url)
            document_id = self.cnt
            self.cnt += 1

            client = ur.urlopen(url)
            page_html = client.read()
            soup = BeautifulSoup(page_html, 'lxml')
            data = soup.body
       
            article = ""
            for p in data.find_all('p'):
                article += p.text
            self.doc.insert({'id':document_id, 'name':document_name, 'content':article})

           

           
            
        except Exception as e:
            print str(e)
            pass

    # def _insert_to_mongodb(self, dict)




if __name__ == '__main__':
    we = WikiExtractor('/home/sue/workspace/independent_study/documentsentimentanalysis/data/test.txt')
    we.get_url()
