#!/usr/bin/env python 

'''
This script gets text data from mongodb and save it in pickle. Text data can be wiki or amazon review text.
The pickle data is used later as input to paragraph2vector model.
Examples:
Preparing a trainning set for paragraph2vector with wiki data data is stored in the "Documents" collection in 
mongodb (running on localhost):
python SaveTextTrainging.py --database_name WikiDb  --data_collection Documents
or a trainning set with Amazon review Text is stored in the "reviews" collection 
python SaveTextTrainging.py --database_name AmazonReview  --data_collection reviews
'''
import pickle
import json 
import os
import numpy as np
from pymongo import MongoClient
import sys
from argparse import ArgumentParser
from TextTrainingData import TextTrainingData

def main():
    parser = ArgumentParser()
    parser.add_argument('--database_name', dest='database_name', choices=['WikiDb', 'AmazonReview'])
    cmdargs = parser.parse_args(sys.argv[1:])
    
    database = cmdargs.database_name
    mongo_client = MongoClient('localhost', 27017)
    db = mongo_client[database]

    data = TextTrainingData(min_word_freq=2)
    base_dir = '/home/suwei/workspace/independent_study/data'
    if database == 'WikiDb':
        i = 0
        for doc in db['Documents'].find({'document_id': {'$exists':True}}):
            data.add_text(doc['document_content'], doc_name=doc['document_name'])

    elif database == 'AmazonReview':
        sys.exit('Wrong argument, use database name WikiDb or AmazonReview')
        
    else:
        sys.exit('Wrong argument, use database name WikiDb or AmazonReview')

    print()
    print('Deleting infrequent tokens...')
    num_positions_deleted, num_tokens_deleted = data.purge_infrequent_tokens()
    print('Deleted {:d} tokens and {:d} positions'.format(num_tokens_deleted, num_positions_deleted))
    print('Total vocab size in the end is {:d}'.format(len(data.id2freq)))
    print('Total text size in the end is {:d}'.format(data.total_words()))


    with open(os.path.join(base_dir, 'text_training_data.pickle'), 'wb') as f:
        pickle.dump(data, f)

    with open(os.path.join(base_dir, 'id2freq.npy'), 'wb') as f:
        np.save(f, np.asarray(data.id2freq, dtype=np.int64))

    with open(os.path.join(base_dir, 'token2id.json'), 'w') as f:
        json.dump(data.token2id, f, indent=4, sort_keys=True)   

if __name__ == '__main__':
    main()



